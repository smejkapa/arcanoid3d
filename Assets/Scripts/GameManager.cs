﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static int Lives = 3;
    public int BrickWidth = 6;
    public int BrickHeight = 4;
    public int BrickDepth = 4;
    public float BrickHSpacing = 2.5f;
    public float BrickVSpacing = 1.5f;
    public float ResetDelay = 1.0f;
    public Text LivesText;
    public Text ScoreText;
    public GameObject EffectText;
    public GameObject GameOver;
    public GameObject YouWon;
    public GameObject BrickPrefab;
    public GameObject BrickUpPrefab;
    public GameObject BrickDownPrefab;
    public GameObject BricksPrefab;
    public GameObject Paddle;
    public GameObject DeathParticles;
    public GameObject BricksGrid;
    public AudioClip PowerUpSound;
    public AudioClip PowerDownSound;
    private List<GameObject> _balls = new List<GameObject>();
    private static int _score;
    private static int _level = 1;
    private AudioSource _audio;

    public static GameManager Instance { get; private set; }

    private GameObject _clonePaddle;
    private int _bricks;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
        Setup();
    }

    private void Setup()
    {
        SetupPaddle();

        LivesText.text = $"Lives: {Lives}";
        AddScore(0);

        _bricks = 0;
        for (int x = 0; x < BrickWidth; x++)
        {
            for (int y = 0; y < BrickHeight; y++)
            {
                for (int z = 0; z < BrickDepth; z++)
                {
                    GameObject brick;
                    var value = Random.value;
                    if (value < 0.10f)
                    {
                        brick = Instantiate(BrickUpPrefab, BricksGrid.transform);
                    }
                    else if (value < 0.20f)
                    //else if (value < 1.0f)
                    {
                        brick = Instantiate(BrickDownPrefab, BricksGrid.transform);
                    }
                    else
                    {
                        brick = Instantiate(BrickPrefab, BricksGrid.transform);
                    }
                    brick.transform.localPosition = new Vector3(x * BrickHSpacing, -y * BrickVSpacing, z * BrickHSpacing);
                    ++_bricks;
                }
            }
        }

        YouWon.GetComponent<Text>().text = $"Level {_level}";
        YouWon.SetActive(true);
        Invoke("HideLvlText", 2.0f);
    }

    private void HideLvlText()
    {
        YouWon.SetActive(false);
    }

    private void DisplayEffect(string effect)
    {
        EffectText.GetComponent<Text>().text = effect;
        EffectText.SetActive(true);
        Invoke("HideEffectText", 2.0f);
    }

    private void HideEffectText()
    {
        EffectText.SetActive(false);
    }

    private void CheckGameOver()
    {
        if (_bricks == 0)
        {
            EffectText.SetActive(false);
            YouWon.GetComponent<Text>().text = $"Level {++_level}";
            YouWon.SetActive(true);
            Time.timeScale = 0.25f;
            Lives += 3;
            Invoke("Reset", ResetDelay);
        }
        else if (Lives <= 0)
        {
            EffectText.SetActive(false);
            GameOver.SetActive(true);
            Time.timeScale = 0.25f;
            Invoke("Reset", ResetDelay);
            _score = 0;
            _level = 1;
            Lives = 3;
        }
    }

    private void Reset()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoseBall()
    {
        for (int i = _balls.Count - 1; i >= 0; i--)
        {
            if (_balls[i] == null || // Destroyed
                _balls[i].GetComponent<Ball>().IsDestroyed) 
            {
                _balls.RemoveAt(i);
            }
        }

        if (_balls.Count == 0)
        {
            --Lives;
            LivesText.text = $"Lives: {Lives}";
            Instantiate(DeathParticles, _clonePaddle.transform.position, Quaternion.identity);
            Destroy(_clonePaddle);
            CheckGameOver();
            Invoke("SetupPaddle", ResetDelay);
        }
    }

    private void SetupPaddle()
    {
        _clonePaddle = Instantiate(Paddle, transform.position, Quaternion.identity);
        Transform ball = _clonePaddle.transform.GetChild(0);
        _balls = new List<GameObject> {ball.gameObject};
    }

    public void DestroyBrick()
    {
        --_bricks;
        CheckGameOver();
    }

    public void ChangeLives(int delta)
    {
        if (delta == -1)
        {
            PowerDownEffects("Life down");
        }
        Lives += delta;
        LivesText.text = $"Lives: {Lives}";
        CheckGameOver();
    }

    public void MultiplyBalls()
    {
        Debug.Log("Multi ball");
        _audio.PlayOneShot(PowerUpSound);
        DisplayEffect("Multi ball");
        var newBalls = new List<GameObject>();
        foreach (GameObject ball in _balls)
        {
            if (ball.GetComponent<Ball>().IsLaunched)
            {
                GameObject newBall = Instantiate(ball);
                newBalls.Add(newBall);
                var rb = newBall.GetComponent<Ball>();
                rb.LaunchBall();
            }
        }

        _balls.AddRange(newBalls);

        foreach (GameObject ball in _balls)
        {
            foreach (GameObject o in _balls)
            {
                Physics.IgnoreCollision(ball.GetComponent<Collider>(), o.GetComponent<Collider>());
            }
        }
    }

    public void BiggerBalls()
    {
        Debug.Log("Big balls");
        _audio.PlayOneShot(PowerUpSound);
        DisplayEffect("Big balls");
        foreach (GameObject ball in _balls)
        {
            if (ball.GetComponent<Ball>().IsLaunched)
            {
                ball.GetComponent<Ball>().ChangeSize(Ball.DeltaStat.More);
            }
        }
    }

    public void SmallerBalls()
    {
        PowerDownEffects("Small balls");
        foreach (GameObject ball in _balls)
        {
            if (ball.GetComponent<Ball>().IsLaunched)
            {
                ball.GetComponent<Ball>().ChangeSize(Ball.DeltaStat.Less);
            }
        }
    }

    public void FasterBalls()
    {
        PowerDownEffects("Fast balls");
        foreach (GameObject ball in _balls)
        {
            if (ball.GetComponent<Ball>().IsLaunched)
            {
                ball.GetComponent<Ball>().ChangeSpeed(Ball.DeltaStat.More);
            }
        }
    }

    public void SlowerBalls()
    {
        DisplayEffect("Slow balls");
        _audio.PlayOneShot(PowerUpSound);
        foreach (GameObject ball in _balls)
        {
            if (ball.GetComponent<Ball>().IsLaunched)
            {
                ball.GetComponent<Ball>().ChangeSpeed(Ball.DeltaStat.Less);
            }
        }
    }

    public void AddScore(int value)
    {
        _score += value;
        ScoreText.text = $"Score: {_score}";
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            SceneManager.LoadScene("Scenes/MenuScene");
        }
        if (Input.GetKeyDown("space"))
        {
            MultiplyBalls();
        }
        if (Input.GetKeyDown("b"))
        {
            BiggerBalls();
        }
        if (Input.GetKeyDown("n"))
        {
            SmallerBalls();
        }
        if (Input.GetKeyDown("j"))
        {
            FasterBalls();
        }
        if (Input.GetKeyDown("k"))
        {
            SlowerBalls();
        }
    }

    void PowerDownEffects(string msg)
    {
        Debug.Log(msg);
        _audio.clip = PowerDownSound;
        _audio.time = 1.0f;
        _audio.Play();
        DisplayEffect(msg);
    }
}
