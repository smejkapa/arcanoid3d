﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public bool IsVisible;

    public void Awake()
    {
        Cursor.visible = IsVisible;
    }
}
