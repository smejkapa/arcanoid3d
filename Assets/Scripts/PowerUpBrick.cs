﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class PowerUpBrick : MonoBehaviour
{
    public GameObject CapsulePrefab;
    public bool IsPowerDown;

    static readonly System.Random _rnd = new System.Random();

    public void OnCollisionEnter(Collision other)
    {
        // Spawn capsule
        GameObject capsule = Instantiate(CapsulePrefab, transform.position, Quaternion.Euler(0, 0, 90));
        capsule.GetComponent<Renderer>().material = GetComponent<Renderer>().material;
        var controller = capsule.GetComponent<CapsuleController>();
        PowerType type;
        if (IsPowerDown)
        {
            type = (PowerType)_rnd.Next((int) PowerType.POWER_DOWNS + 1, (int) PowerType.Count);
        }
        else
        {
            type = (PowerType)_rnd.Next(0, (int)PowerType.POWER_DOWNS);
        }
        controller.Launch(type);
    }
}
