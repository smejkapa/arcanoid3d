﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickController : MonoBehaviour
{
    public GameObject DestroyedParticle;
    public GameObject DestroyedSound;
    private bool _isDestroyed;

    void OnCollisionEnter(Collision other)
    {
        if (!_isDestroyed)
        {
            _isDestroyed = true;
            Instantiate(DestroyedSound, transform.position, Quaternion.identity);
            Instantiate(DestroyedParticle, transform.position, Quaternion.identity);
            GameManager.Instance.DestroyBrick();
            GameManager.Instance.AddScore(10);
            Destroy(gameObject);
        }
    }
}
