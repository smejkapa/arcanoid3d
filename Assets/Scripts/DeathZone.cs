﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            var ball = other.GetComponent<Ball>();
            ball.IsDestroyed = true;
            ball.StopAllCoroutines();
            GameManager.Instance.LoseBall();
        }
        Destroy(other.gameObject);
    }
}
