﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Configuration;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum DeltaStat
    {
        Less = -1,
        More = 1
    }

    private enum Size
    {
        Small,
        Normal,
        Big
    }

    private enum Speed
    {
        Slow,
        Normal,
        Fast
    }

    public Vector3 BallInitialVelocity = new Vector3(600, 600, 300);
    private Rigidbody _rigidbody;
    public bool IsLaunched { get; private set; }
    private Size _ballSize = Size.Normal;
    private Speed _ballSpeed = Speed.Normal;
    public bool IsDestroyed;

    public AudioClip LaunchSound;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && !IsLaunched)
        {
            GetComponent<AudioSource>().PlayOneShot(LaunchSound);
            LaunchBall();
        }
    }

    public void LaunchBall()
    {
        transform.parent = null;
        IsLaunched = true;
        _rigidbody.isKinematic = false;
        _rigidbody.AddForce(BallInitialVelocity);
    }

    public void ChangeSize(DeltaStat deltaSize)
    {
        // Clamp between Smaller and Bigger
        var ballSize = Math.Max((int)Size.Small, Math.Min((int)Size.Big, (int) _ballSize + (int) deltaSize));
        var newBallsize = (Size) ballSize;
        if (_ballSize == newBallsize)
            return;

        _ballSize = newBallsize;
        var size = 1.0f;
        switch (_ballSize)
        {
            case Size.Big:
                size = 2.0f;
                break;
            case Size.Normal:
                size = 1.0f;
                break;
            case Size.Small:
                size = 0.5f;
                break;
        }

        iTween.ScaleTo(gameObject, new Vector3(size, size, size), 1.0f);
    }

    public void ChangeSpeed(DeltaStat deltaSpeed)
    {
        // Clamp between Smaller and Bigger
        var ballSpeed = Math.Max((int)Speed.Slow, Math.Min((int)Speed.Fast, (int)_ballSpeed + (int)deltaSpeed));
        var newBallSpeed = (Speed)ballSpeed;
        if (_ballSpeed == newBallSpeed)
            return;

        _ballSpeed = newBallSpeed;
        var multiplier = deltaSpeed == DeltaStat.Less ? 0.67f : 1.5f;

        _rigidbody.velocity *= multiplier;
    }

    public void OnCollisionEnter(Collision collision)
    {
        GetComponent<AudioSource>().Play();
    }
}
