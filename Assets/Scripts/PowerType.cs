﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum PowerType
    {
        // Up
        MultiBall,
        SlowBall,
        BigBall,
        // Down
        POWER_DOWNS,
        FastBall,
        SmallBall,
        LifeDown,
        Count
    }
}
