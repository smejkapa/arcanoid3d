﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class CapsuleController : MonoBehaviour
{
    public GameObject DamageParticles;

    public Material BigBallMat;
    public Material FastBallMat;
    public Material LifeDownMat;
    public Material MultiBallMat;
    public Material SlowBallMat;
    public Material SmallBallMat;

    private Action _execMethod;
    private PowerType _type;
    private bool _isExecuted;

    public void Launch(PowerType type)
    {
        _type = type;
        var rb = GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(0, -300, 0));
        Setup();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Paddle") && !_isExecuted)
        {
            _isExecuted = true;
            _execMethod();
            Destroy(gameObject);
        }
    }

    private void Setup()
    {
        var rndr = GetComponent<Renderer>();
        switch (_type)
        {
            case PowerType.BigBall:
                rndr.material = BigBallMat;
                _execMethod = GameManager.Instance.BiggerBalls;
                break;
            case PowerType.FastBall:
                rndr.material = FastBallMat;
                _execMethod = GameManager.Instance.FasterBalls;
                break;
            case PowerType.LifeDown:
                rndr.material = LifeDownMat;
                _execMethod = () => GameManager.Instance.ChangeLives(-1);
                Instantiate(DamageParticles, transform.position, Quaternion.identity);
                break;
            case PowerType.MultiBall:
                rndr.material = MultiBallMat;
                _execMethod = GameManager.Instance.MultiplyBalls;
                break;
            case PowerType.SlowBall:
                rndr.material = SlowBallMat;
                _execMethod = GameManager.Instance.SlowerBalls;
                break;
            case PowerType.SmallBall:
                rndr.material = SmallBallMat;
                _execMethod = GameManager.Instance.SmallerBalls;
                break;
            case PowerType.POWER_DOWNS:
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
