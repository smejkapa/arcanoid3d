﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public float PaddleSpeed = 1.0f;

    private Vector3 _playerPos = new Vector3(0, -9.5f, 0);

    public void Update()
    {
        float xPos = transform.position.x;
        xPos += Input.GetAxis("Mouse X") * PaddleSpeed;
        float zPos = transform.position.z;
        zPos -= Input.GetAxis("Mouse Y") * PaddleSpeed;
        _playerPos = new Vector3(Mathf.Clamp(xPos, -8f, 8f), _playerPos.y , Mathf.Clamp(zPos, -5f, 5f));
        transform.position = _playerPos;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            iTween.ShakePosition(Camera.main.gameObject, new Vector3(0.15f, 0.15f), 0.5f);
            GetComponent<AudioSource>().Play();

            //var rb = collision.gameObject.GetComponent<Rigidbody>();
            //var dot = Vector3.Dot(rb.velocity.normalized, new Vector3(0, 1, 0));
            //var angle = Mathf.Acos(dot) * Mathf.Rad2Deg;
            //Debug.Log($"Hit angle = {angle}");
        }
    }
}
