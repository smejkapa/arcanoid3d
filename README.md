# Arkanoid 3D

A different take on classic game Arkanoid. Made in Unity 3D as one of three games submitted as a semestral project to [Game Development Middleware](https://gamedev.cuni.cz/study/courses-history/courses-2016-2017/game-development-middleware-winter-201617/) at Faculty of Mathematics and Physics, Charles University.

In this version, the player looks from under the paddle and the paddle can move in two dimensions (instead of only one in the classical version). The game also includes several power ups and power downs.

## Getting started

All you need is Unity 2017.1.0f3 and [Git LFS](https://git-lfs.github.com/) since the binary assets are stored there.

## How to play

* The paddle is controlled by the mouse. 
* Left click shoots the ball. 
* Esc returns to the menu

There are also several debug commands to experiment with the game and to see how different powerups work.

* B - bigger balls
* N - smaller balls
* J - faster balls
* K - slower balls

Green bricks are power-ups, red bricks are power downs. From these bricks (when destroyed) a capsule is dropped which can be picked up by the paddle.

## Development notes

Compared to 2D physics in Unity, 3D physics in Unity works relatively nice in my experience.

One of my long missing features in Unity is the inheritance of prefabs. It would be nice to be able to derive `PowerUpBrick` from `Brick` similarly to how Unreal Engine 4 handles blueprints.

Probably the biggest challenge in this 3D version was how to provide enough feedback visual clues what is the depth and speed of the ball on the screen. 
This seems to be the biggest issue still and makes the game rather hard at times. I also experimented with an idea to project the depth of the ball to sides of the box where we play, which did not prove that useful.
A more useful approach may be to project the ball's position to the plane where the paddle is. This may, however, make the game too easy.

The ball movement is not ideal, it would be great to implement spin and to better control the speed, however, the game is fully playable.

## Acknowledgements

The materials on objects used in the game are from Unity asset store, sounds are from freesound.org, and the music is from http://incompetech.com/music/royalty-free/music.html.